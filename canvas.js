window.onload = function() {
	initTools();
	initBtns();
	initCanvas();
	initLineWidth();
	ctx.fillStyle = "rgb(255,255,255)";
	ctx.fillRect (0, 0, 1200, 800);
	cPush();
}

var canvas1 = document.getElementById("canvas1");
var ctx = canvas1.getContext('2d');
var lineWidths = document.querySelectorAll(".lineWidth");
var clickedLineWidthId = "five";
var selectedLineWidth = 5;
var tools = document.querySelectorAll(".tool");
var btns = document.querySelectorAll(".btn");
var clickedToolId = "brush";
var x,y;
var drawing = false;
var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
var img;
var mousePos1;
ctx.lineWidth = 5;

/* color picker begin */
var colorBlock = document.getElementById('color-block');
var ctx1 = colorBlock.getContext('2d');
var width1 = colorBlock.width;
var height1 = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctx2 = colorStrip.getContext('2d');
var width2 = colorStrip.width;
var height2 = colorStrip.height;

var colorLabel = document.getElementById('color-label');

var drag = false;
var rgbaColor = 'rgba(0,0,0,1)';

ctx1.rect(0, 0, width1, height1);
fillGradient();

ctx2.rect(0, 0, width2, height2);
var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
ctx2.fillStyle = grd1;
ctx2.fill();

function click(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx2.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  fillGradient();
}

function fillGradient() {
  ctx1.fillStyle = rgbaColor;
  ctx1.fillRect(0, 0, width1, height1);

  var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
  grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
  grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
  ctx1.fillStyle = grdWhite;
  ctx1.fillRect(0, 0, width1, height1);

  var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
  grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
  grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
  ctx1.fillStyle = grdBlack;
  ctx1.fillRect(0, 0, width1, height1);
}

function mousedown(e) {
  drag = true;
  changeColor(e);
}

function mousemove(e) {
  if (drag) {
    changeColor(e);
  }
}

function mouseup(e) {
  drag = false;
}

function changeColor(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx1.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  colorLabel.style.backgroundColor = rgbaColor;
  if(clickedToolId != "eraser")
	ctx.strokeStyle = rgbaColor;
}

colorStrip.addEventListener("click", click, false);

colorBlock.addEventListener("mousedown", mousedown, false);
colorBlock.addEventListener("mouseup", mouseup, false);
colorBlock.addEventListener("mousemove", mousemove, false);

/* color picker end */

function initLineWidth() {
	for(var i = 0 ; i < lineWidths.length ; i++)
	{
		lineWidths[i].addEventListener("click", function(){
			document.getElementById(clickedLineWidthId).classList.remove("clicked");
			this.classList.add("clicked");
			clickedLineWidthId = this.id;
			ctx.lineWidth = this.width;
			selectedLineWidth = this.width;
		})
	}
}

function initTools() {
	for(var i = 0 ; i < tools.length ; i++) {
		tools[i].addEventListener("click", function(){
			document.getElementById(clickedToolId).classList.remove("clicked");
			this.classList.add("clicked");
			clickedToolId = this.id;
		})
		if(tools[i].id=="eraser"){
			tools[i].addEventListener("click", function (){
				ctx.lineWidth = 40;
				ctx.strokeStyle = "white";
				canvas1.style.cursor = "cell";
				imageLoader.style.display = "none";
			})
		}
		else if(tools[i].id == "brush") {
			tools[i].addEventListener("click", function (){
				ctx.lineWidth = selectedLineWidth;
				ctx.strokeStyle = rgbaColor;
				canvas1.style.cursor = "crosshair";
				imageLoader.style.display = "none";
			})
		}
		else if(tools[i].id == "text") {
			tools[i].addEventListener("click", function (){
				canvas1.style.cursor = "text";
				imageLoader.style.display = "none";
			})	
		}
		else if(tools[i].id =="picture"){
			tools[i].addEventListener("click", function (){
				imageLoader.style.display = "block";
				canvas1.style.cursor = "copy";
			})	
		}
		else if(tools[i].id == "rectangle"){
			tools[i].addEventListener("click", function (){
				imageLoader.style.display = "none";
				canvas1.style.cursor = "se-resize";
				ctx.strokeStyle = rgbaColor;
			})
		}
		else if(tools[i].id == "circle"){
			tools[i].addEventListener("click", function (){
				imageLoader.style.display = "none";
				canvas1.style.cursor = "se-resize";
				ctx.strokeStyle = rgbaColor;
			})
		}
		else if(tools[i].id == "triangle"){
			tools[i].addEventListener("click", function (){
				imageLoader.style.display = "none";
				canvas1.style.cursor = "se-resize";
				ctx.strokeStyle = rgbaColor;
			})
		}
	}
}

/* redo undo begin*/
var cPushArray = new Array();
var cStep = -1;
var ctx;
// ctx = document.getElementById('myCanvas').getContext("2d");
	
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas1').toDataURL());
}
function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}
/* redo undo end*/

function initBtns() {
	document.getElementById("download").addEventListener("click",function (){
		var link = document.getElementById("download_link");
		link.href = canvas1.toDataURL();
		link.download = "image.png";
	})
	document.getElementById("redo").addEventListener("click",function (){
		cRedo();
	})
	document.getElementById("undo").addEventListener("click",function (){
		cUndo();
	})
	document.getElementById("refresh").addEventListener("click",function (){
		ctx.fillStyle = "rgb(255,255,255)";
		ctx.fillRect (0, 0, 1200, 800);
		cPush();
	})
}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };   
};
function mouseMove(evt) {
  var mousePos = getMousePos(canvas1, evt);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.stroke();
};

function initCanvas() {
	canvas1.addEventListener('mousedown', function(evt) {
		mousePos1 = getMousePos(canvas1, evt);
		evt.preventDefault();
		ctx.beginPath();
		ctx.moveTo(mousePos1.x, mousePos1.y); 
		if(clickedToolId=="brush" || clickedToolId=="eraser")
			drawing = true;
		else if(clickedToolId=="text"){
			var size1 = document.getElementById("size");
			var size2 = size1.options[size1.selectedIndex].value;
			var font1 = document.getElementById("font");
			var font2 = font1.options[font1.selectedIndex].value;
			var content = document.getElementById("content");
			ctx.font = size2+"px "+font2;
			ctx.fillStyle = rgbaColor;
			ctx.fillText(content.value,mousePos1.x,mousePos1.y);
		}
		else if(clickedToolId=="picture"){
			ctx.drawImage(img,mousePos1.x-img.width/2,mousePos1.y-img.height/2);
		}
	});
	
	canvas1.addEventListener('mousemove',function(evt){
		if(drawing)
			mouseMove(evt);			
	},false);
	
	canvas1.addEventListener('mouseup', function(evt) {
		drawing = false;
		if(clickedToolId == "rectangle"){
			ctx.strokeRect(Math.min(mousePos1.x, getMousePos(canvas1,evt).x), Math.min(mousePos1.y,getMousePos(canvas1,evt).y), Math.abs(getMousePos(canvas1,evt).x - mousePos1.x), Math.abs(getMousePos(canvas1,evt).y - mousePos1.y));
		}
		else if(clickedToolId == "triangle"){
			ctx.moveTo((mousePos1.x+getMousePos(canvas1,evt).x)/2, Math.min(mousePos1.y,getMousePos(canvas1,evt).y));
			ctx.lineTo(Math.min(mousePos1.x, getMousePos(canvas1,evt).x),Math.max(mousePos1.y,getMousePos(canvas1,evt).y));
			ctx.stroke();
			ctx.lineTo(Math.max(mousePos1.x, getMousePos(canvas1,evt).x),Math.max(mousePos1.y,getMousePos(canvas1,evt).y));
			ctx.stroke();
			ctx.lineTo((mousePos1.x+getMousePos(canvas1,evt).x)/2, Math.min(mousePos1.y,getMousePos(canvas1,evt).y));
			ctx.stroke();
		}
		else if(clickedToolId =="circle") {
			ctx.moveTo((mousePos1.x+getMousePos(canvas1,evt).x)/2+Math.abs(getMousePos(canvas1,evt).x - mousePos1.x)/2,(mousePos1.y+getMousePos(canvas1,evt).y)/2);
			ctx.arc((mousePos1.x+getMousePos(canvas1,evt).x)/2,(mousePos1.y+getMousePos(canvas1,evt).y)/2,Math.abs(getMousePos(canvas1,evt).x - mousePos1.x)/2,0,Math.PI*2,true);
			ctx.stroke();
		}
		cPush();
	}, false);
}

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        img = new Image();
        img.onload = function(){
            
        }
        img.src = event.target.result;
		img.setAttribute("crossOrigin",'Anonymous');
    }
    reader.readAsDataURL(e.target.files[0]);     
}