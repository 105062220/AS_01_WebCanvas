# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Functions details
* 左邊是畫布，右邊則有顏色選擇、線條粗細、文字內容、工具組
* 顏色選擇、線條粗細可套用在Brush、Circle、Triangle、Rectangle、Text
* 文字內容包含輸入、文字大小、字型
* 在畫布上的游標會根據當前工具做改變
* 工具組  
1.Brush: 在畫布上畫出線條  
2.Eraser: 擦掉畫布上的顏色  
3.Circle: 在畫布上往任意方向拉出圓形的邊框  
4.Triangle: 在畫布上往任意方向拉出三角形的邊框  
5.Rectangle: 在畫布上往任意方向拉出長方形的邊框  
6.Text: 輸入完文字內容後，在畫布上貼上文字  
7.Picture: 出現上傳圖片的按鈕，上傳圖片後，在畫布上貼上圖片  
8.Refresh: 將整個畫布清空  
9.Download: 下載目前的畫布(.png)  
10.Undo: 取消上一個動作  
11.Redo: 重做被Undo的動作


